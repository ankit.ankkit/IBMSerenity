package com.test.toll.glue;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.utilities.GenericMethods;
import com.test.toll.utilities.RestServicesUtil;
import com.toll.exception.TollExceptions;

import cucumber.api.java.en.Given;

public class RestServices {

	private String userId = "130";
	private String newUserCreated;
	private String passordForNewUser;

	final Logger log = LoggerFactory.getLogger(ISIMServiceCentre.class);

	@Given("^create user with API for \"([^\"]*)\" using \"([^\"]*)\" and validate response is \"(\\d+)\"$")
	public void createUser(String scenarioName, String jasonTemplate, int expRespcode) {
		String getValueOfJsonObject = "id";
		try {

			String bearerToken = RestServicesUtil.getBearerToken("Admin");
			String httpUrl = GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("createUser");
			System.out.println(scenarioName);
			if (scenarioName.toLowerCase().contains("internal")) {
				httpUrl = httpUrl.replace("{$userID}", GenericMethods.getProperty("internalUserId"));
			} else {
				httpUrl = httpUrl.replace("{$userID}", GenericMethods.getProperty("externalUserId"));
			}
			log.info(httpUrl);
			HttpURLConnection con = RestServicesUtil.sendHttpRequestAndValidateResponseCode(bearerToken, scenarioName, jasonTemplate,
					httpUrl, "POST", "Ideas", expRespcode);
			int responseCode = RestServicesUtil.gethttpResponseCode(con, expRespcode);
			Assert.assertTrue("Verifying the http Response code", responseCode == expRespcode);

			// Getting objects only if status code is 200
			String id = RestServicesUtil.gethttpResponseJasonVal(con, getValueOfJsonObject, expRespcode);
			System.out.println(id);
		} catch (Exception e) {
			throw new TollExceptions("error while sending request to create user " + e.getMessage());
		}
	}

	@Given("user send request to modify his details for scenario \"([^\"]*)\" and using Jason template \"([^\"]*)\"")
	public void modifyUserDetails(String scenarioName, String jasonTemplate) throws IOException {
		String bearerToken = RestServicesUtil.getBearerToken("Admin");
		log.info("Sending request to modify the user");
		log.info("User Id is" + userId);
		HttpURLConnection con;
		try {
			String httpUrl = GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("modifyUserAPIurl")
					+ this.userId;
			System.out.println(httpUrl);
			con = RestServicesUtil.sendHttpRequestAndValidateResponseCode(bearerToken, scenarioName, jasonTemplate, httpUrl, "PUT",
					"Ideas", 200);
			String username = RestServicesUtil.gethttpResponseJasonVal(con, "id", 200);
			System.out.println(username);

		} catch (Exception e) {
			throw new TollExceptions("error while modifying user" + e.getMessage());
		}

	}

/*	@Given("admin send request to disable user")
	public void disableuser() throws IOException {
		String bearerToken = RestServicesUtil.getBearerToken("Admin");
		log.info("Sending request to modify the user");
		log.info("User Id is" + userId);
		HttpURLConnection con = null;
		try {
			String httpUrl = GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("modifyUserAPIurl")
					+ this.userId;
			System.out.println(httpUrl);
			// con = sendHttpRequestAndValidateResponseCode(bearerToken,scenarioName,
			// jasonTemplate, httpUrl, "PUT" , "Ideas", 200);
			String username = RestServicesUtil.gethttpResponseJasonVal(con, "id", 200);
			System.out.println(username);

		} catch (Exception e) {
			throw new TollExceptions("error while modifying user" + e.getMessage());
		}

	}*/

	
	@Given("user send request to modify his details copy")
	public void modifyUserDetailsCopy() throws IOException {
		String BearerToken = RestServicesUtil.getBearerToken("Admin");
		log.info("Sending request to modify the user");
		System.out.println("User Id is" + userId);
		URL obj = new URL("https://ip-172-31-38-182.ap-southeast-2.compute.internal:9343/igi/v2/agc/users/136");
		String urlParameters = "{\r\n" + "              \"schemas\": [\r\n"
				+ "                             \"urn:ietf:params:scim:schemas:core:2.0:User\",\r\n"
				+ "                             \"urn:ibm:params:scim:schemas:extension:bean:agc:2.0:User\"\r\n"
				+ "              ],\r\n" + "              \"name\": {\r\n" + "              },\r\n"
				+ "              \"userName\": \"GROUPTEST12134\",\r\n"
				+ "              \"password\": \"Asdfg12@sdd\",\r\n" + "              \"emails\": [{\r\n"
				+ "                             \"value\": \"steve.hok123@awe.com\"\r\n" + "              }],\r\n"
				+ "              \"phoneNumbers\": [{\r\n"
				+ "                            \"value\": \"70422330098\"\r\n" + "              }],\r\n" + "  \r\n"
				+ "              \"urn:ibm:params:scim:schemas:extension:bean:agc:2.0:User\": {\r\n" + "   \r\n"
				+ "              }\r\n" + "              }";

		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setDoOutput(true);
		con.setRequestMethod("PUT");
		con.setRequestProperty("Content-Type", "application/scim+json");
		con.setRequestProperty("Accept", "application/scim+json");
		con.setRequestProperty("realm", "Ideas");
		con.setRequestProperty("Authorization", "Bearer " + BearerToken);

		// For POST only - START
		con.setDoOutput(true);
		log.info("Sending Put request");
		OutputStream os = con.getOutputStream();
		os.write(urlParameters.getBytes());
		os.flush();

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);
		Assert.assertTrue(responseCode == 200);

	}

	@Given("user changes the password using API")
	public void changePassword() throws KeyManagementException, NoSuchAlgorithmException, MalformedURLException,
			ProtocolException, IOException {
		String bearerToken = RestServicesUtil.getBearerToken("Ideas");
		Assert.assertFalse("Not able to change password the user", false);
	}

	

}
