package com.test.toll.glue;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.utilities.GenericMethods;
import com.test.toll.utilities.RestServicesUtil;
import com.toll.exception.TollExceptions;

import cucumber.api.java.en.Given;

public class TaniaInProgress {


	final Logger log = LoggerFactory.getLogger(TaniaInProgress.class);
	
	@Given("admin send request to disable user")
    public void disableuser1() throws IOException {
          String bearerToken = RestServicesUtil.getBearerToken("Admin");
          System.out.println(bearerToken);
          log.info("Sending request to disable the user");
          HttpURLConnection con = null;
          try {
                 String httpUrl = GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("disableuserAPIurl");
                 System.out.println(httpUrl);
                 
                 con = RestServicesUtil.sendHttpRequestAndValidateResponseCode(bearerToken,"","", httpUrl, "PUT" , "Ideas", 200);
          
                 String response = RestServicesUtil.gethttpResponseJasonVal(con, "id", 200);
                 System.out.println(response);

          } catch (Exception e) {
                 throw new TollExceptions("error while disabling user" + e.getMessage());
          }

    }
    
    @Given("admin send request to enable user")
    public void enableuser() throws IOException {
          String bearerToken = RestServicesUtil.getBearerToken("Admin");
          log.info("Sending request to enable the user");
          HttpURLConnection con = null;
          try {
                 String httpUrl = GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("enableuserAPIurl");
                 System.out.println(httpUrl);
                 
                 con = RestServicesUtil.sendHttpRequestAndValidateResponseCode(bearerToken,"","", httpUrl, "PUT" , "Ideas", 200);
          
                 String response = RestServicesUtil.gethttpResponseJasonVal(con, "id", 200);
                 System.out.println(response);

          } catch (Exception e) {
                 throw new TollExceptions("error while enabling user" + e.getMessage());
          }

    }


	
	
	

}
