package com.test.toll.glue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.steps.CommonSteps;
import com.test.toll.steps.ISAMHomePageSteps;
import com.toll.exception.TollExceptions;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class Common {

	final Logger log = LoggerFactory.getLogger(ISAMHomePageSteps.class);
	
	@Steps
	CommonSteps commonSteps;
	
	@Given("^user enters username and password for page \"([^\"]*)\"")
	public void enterCredentialsforPage(String pageName){
		log.info("Entering user name and password");
		try {
			commonSteps.enterCredentials(pageName);
		} catch (InterruptedException e) {
			throw new TollExceptions("Eception while entering login name and password to page "+ pageName + e.getMessage());
		}
	}
}
