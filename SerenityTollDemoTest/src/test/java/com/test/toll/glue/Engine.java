package com.test.toll.glue;

import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.utilities.GenericMethods;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.serenitybdd.core.Serenity;

public class Engine {

	final Logger log = LoggerFactory.getLogger(Engine.class);

	@Before
	public String setUp() throws FileNotFoundException {
		log.info("Before test ........");
		System.setProperty("env", "stage");
		log.info(System.getProperty("env"));
		GenericMethods.loadPropertyFiles(System.getProperty("env"));
		Serenity.initializeTestSession();
		/*
		 * log.debug("Hi, {}", "ANkit Garg");
		 * log.info("Welcome to the HelloWorld example of Logback.");
		 * log.warn("Dummy warning message."); log.error("Dummy error message.");
		 */
		return "if (window.screen) {window.moveTo(0, 0);window.resizeTo(window.screen.availWidth,window.screen.availHeight)}";

	}

	@After
	public void tearDown() {
		log.info("After test .......");
	}

}
