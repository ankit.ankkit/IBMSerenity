package com.test.toll.glue;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.steps.ISAMHomePageSteps;
import com.test.toll.utilities.ExcelDrivenTest;
import com.test.toll.utilities.GenericMethods;
import com.toll.exception.TollExceptions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class ISAMBasicGlue {

	final Logger log = LoggerFactory.getLogger(ISAMHomePageSteps.class);

	@Steps
	ISAMHomePageSteps isamHomePage;

	@Given("^user navigates to IGI page \"([^\"]*)\"")
	public void navigatesTo(String scenarioName) {
		try {
			Serenity.setSessionVariable("scenarioName").to(scenarioName);
			isamHomePage.navigateToUrl(GenericMethods.getProperty("baseUrl")+GenericMethods.getProperty("igiHomePage"));
			Thread.sleep(2000);
		} catch (Exception e) {
			throw new TollExceptions("Error while naviating to url " + e.getMessage());
		}
	}

	@Given("^user navigates to page \"([^\"]*)\" for scenario \"([^\"]*)\"")
	public void navigatesTo(String url, String scenarioName) {
		try {
			Serenity.setSessionVariable("scenarioName").to(scenarioName);
			isamHomePage.navigateToUrl(url);
			Thread.sleep(2000);
		} catch (Exception e) {
			throw new TollExceptions("Error while naviating to url " + url + e.getMessage());
		}
	}

	@Given("^user enters the username and password")
	public void enterCredentials() throws Throwable {
		System.out.println("Entering user name and password");
		isamHomePage.enterCredentials();
	}


	
	@Given("^Verify the HTTP Headers")
	public void verifyTheHeaders() throws Throwable {
		isamHomePage.verifyHeaders();
	}

	@Given("Verify the result")
	public void verfiyIDMHomePage() {
		System.out.println("Verifying the result");
		isamHomePage.verifyHomePage();
	}

	@Given("Verify Tabs displayed at home page")
	public void verifyDefaultTabs() throws Throwable {
		isamHomePage.verifydefaultTabs();
	}

	@Then("click on button \"([^\"]*)\"")
	public void clickOnButton(String buttonName) throws InterruptedException {
		isamHomePage.ClickLogoutButton();
	}

	@Then("User session should end")
	public void verifyUserSession() throws InterruptedException {
		isamHomePage.VerifySession();
	}

	@Then("^user logout by using url \"([^\"]*)\"$")
	public void logout(String url) {
		try {
			isamHomePage.logout(url);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
