package com.test.toll.glue;

import java.awt.AWTException;
import java.net.HttpURLConnection;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.pages.IGIAdminConsole;
import com.test.toll.pages.IGIServiceCentre;
import com.test.toll.pages.ISAMHomePage;
import com.test.toll.pages.ISAMRepo;
import com.test.toll.utilities.ExcelDrivenTest;
import com.test.toll.utilities.GenericMethods;
import com.test.toll.utilities.RestServicesUtil;
import com.toll.exception.TollExceptions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;

public class InProgress {

	public String userName;
	public String password;
	private String userId;
	private String newUserCreated;
	private String passordForNewUser;

	final Logger log = LoggerFactory.getLogger(InProgress.class);

	IGIServiceCentre IGISrviceCentre;
	IGIAdminConsole igiConsolePageResources;
	ISAMRepo getisamreporesources;
	ISAMHomePage getisamhomepageRes;
	
	
	@Then("^Login to security access manager in ISAM$")
    public void loginToSecurityDomainInISAM() throws Throwable {
          log.info("Entering user name and password in Security Domain in ISAM");
    
          Serenity.sessionVariableCalled("scenarioName").toString();
    	/* String homeuserName = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
                       "UserID_IsamSec");
          String homeuserpassword = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
                       "Pass_IsamSec");
          String SearchuserID = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
                       "SearchUserId");          
          System.out.println(homeuserpassword);*/
     
   		Thread.sleep(8000);
   		getisamreporesources.getDriver().switchTo().frame("wpm__frame_id").switchTo().frame(1);
   		
   		List<WebElement> frameCount = getisamreporesources.getDriver().findElements(By.tagName("iframe"));
   		System.out.println(frameCount);
   		
   		//getisamreporesources.getDriver().findElement(By.xpath("//form[@name='formone']//input[@id='userid']")).click();
          
   		/*
    	KeyBoard keyboard = new KeyBoard();

       	getisamhomepageRes.loginName.click();
    	KeyBoard.type("sec_master");
    	
    	Thread.sleep(60000);
*/    	//getisamreporesources.HomeloginPassword.click();
    	
    	/*
    	Actions a= new Actions(getisamreporesources.getDriver());
    	a.keyDown(Keys.TAB).perform();
    	
    	
		WebElement element = IGISrviceCentre.getDriver().findElement(By.xpath("//input[@id='password']"));
		Actions actions = new Actions(IGISrviceCentre.getDriver());
		actions.moveToElement(element).click().perform();	
		Thread.sleep(20000);
		
    	

    	
    	Thread.sleep(2000);
    	KeyBoard.type("P@ssw0rd");
    	
    	
    	
    	
    	
    	
   		
        
         // getisamreporesources.SignOnButton.click();
          
          //getisamreporesources.homeloginAndSearch("sec_master","P@ssw0rd","ABC");
          */
	}

	
	
	
	
	@Given("Verify user details")
	public void verifyUserDetails() throws InterruptedException, AWTException {

		System.out.println("Inside verify user details");	
		Thread.sleep(20000);
					
		WebElement element = IGISrviceCentre.getDriver().findElement(By.xpath("//span[contains(.,'Access Governance Core')and (@class='v-button-caption')]"));
		Actions actions = new Actions(IGISrviceCentre.getDriver());
		actions.moveToElement(element).click().perform();	
		Thread.sleep(20000);
		
		/*//Click on filter button
		WebElement btFilterIcon = IGISrviceCentre.getDriver().findElement(By.xpath("//img[contains(@src,'filter_icon.png')]"));
		actions.moveToElement(btFilterIcon).click().perform();

		Thread.sleep(4000);
		igiConsolePageResources.textSearchIdentity.click();
		Thread.sleep(5000);

		 Robot robot = new Robot();
		 robot.keyPress(KeyEvent.VK_A);
	     Thread.sleep(1000);
	     robot.keyPress(KeyEvent.VK_U);
	     Thread.sleep(1000);
	     robot.keyPress(KeyEvent.VK_T);
	     Thread.sleep(1000);
		 robot.keyPress(KeyEvent.VK_T);
	     Thread.sleep(1000);
	     robot.keyPress(KeyEvent.VK_E);
	     Thread.sleep(1000);
	     robot.keyPress(KeyEvent.VK_S);
	     Thread.sleep(1000);
	     robot.keyPress(KeyEvent.VK_T);
	     Thread.sleep(1000);
	     robot.keyPress(KeyEvent.VK_9);
	     
	     //Click on Search
	     Thread.sleep(1000);
		WebElement btSearch = IGISrviceCentre.getDriver().findElement(By.xpath("//span[contains(.,'Search') and @class='v-button-caption']"));
		actions.moveToElement(btSearch).click().perform();
		Thread.sleep(3000);
		
		//Click on checkbox	
		WebElement itemSearched = IGISrviceCentre.getDriver().findElement(By.xpath("//div[@class='v-grid-tablewrapper']//table//tr[contains(@class,'grid-row-has-data')]//input[@type='checkbox']"));
		actions.moveToElement(itemSearched).click().perform();
		 Thread.sleep(3000);
		
		//Get Master UID and other details from first table;
		
		 List<WebElement> userAttributes = IGISrviceCentre.getDriver().findElements(By.xpath(" //div[@id='edit_pane']//div[@class='v-panel-content v-panel-content-DefaultGroup v-scrollable']//div[@class='v-gridlayout-slot']"));
		System.out.println(userAttributes.size());
				

			for(int i=0;i<userAttributes.size();i++) {
			userAttributes.get(i).click();
			String y= userAttributes.get(i).getAttribute("value");
			String x= userAttributes.get(i).getText();
			System.out.println(y +"-----------------" + x);

			1.	x=$x("//div[@id='edit_pane']//div[@class='v-panel-content v-panel-content-DefaultGroup v-scrollable']//div[@class='v-gridlayout-slot']")
			2.	y=x[5].childNodes
			3. y[0].value
			
		}
		
*/		
		//************Verifying accounts tab************
		actions.moveToElement(igiConsolePageResources.accountsTab).click().perform();
		Thread.sleep(3000);
		//Comment {AG}: We will add this later once My Toll is integrated
			
			//************Verifying Monitor tab************

		actions.moveToElement(igiConsolePageResources.monitorTab).click().perform();
		Thread.sleep(3000);

		actions.moveToElement(igiConsolePageResources.outEventsTab).click().perform();
		Thread.sleep(3000);

		List<WebElement> outEvents = IGISrviceCentre.getDriver().findElements(By.xpath("//div[@class='v-grid-tablewrapper']//tr[contains(@class,'v-grid-row-focused')]//td"));
		
		String actualAccountId = outEvents.get(2).getAttribute("textContent");
		Assert.assertEquals("Checking the Acccount Id on Monitor Out events tab in IGI", "AutoDemoTest12", actualAccountId);
		
		

		String actualOperation = outEvents.get(4).getAttribute("textContent");
		Assert.assertEquals("Checking the Operation", "Create Account", actualOperation);
		
		

		String actualStatus = outEvents.get(5).getAttribute("textContent");
		Assert.assertEquals("Checking the status Monitor Out events tab in IGI", "Success", actualStatus);
		
		

		String actualERCStatus = outEvents.get(6).getAttribute("textContent");
		Assert.assertEquals("Checking the ERCStatus on Monitor Out events tab in IGI", "Success", actualERCStatus);
		

		
	}
	
	
	@Given("^get user details for user \"([^\"]*)\" using API using \"([^\"]*)\"$")
	public void getUserDetails(String userDetail, String jasonTemplate) {
		try {
			int x=200;
			String scenarioName = "GetUserDetails";
			String bearerToken = RestServicesUtil.getBearerToken("Admin");
			String httpUrl = GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("getUserDetails");
			HttpURLConnection con = RestServicesUtil.sendHttpRequestAndValidateResponseCode(bearerToken, scenarioName, jasonTemplate,
					httpUrl, "POST", "Ideas", x);

			RestServicesUtil.gethttpResponseJasonVal2(con, "totalResults", x);

		} catch (Exception e) {
			throw new TollExceptions("error while sending request to create user " + e.getMessage());
		}
	}
	
/*	@Given("^get user details for user \"([^\"]*)\" using API using \"([^\"]*)\"$")
	public void roleChange(String userDetail, String jasonTemplate) {
		try {
			int x=200;
			String scenarioName = "GetUserDetails";
			String bearerToken = RestServicesUtil.getBearerToken("Admin");
			String httpUrl = GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("getUserDetails");
			HttpURLConnection con = RestServicesUtil.sendHttpRequestAndValidateResponseCode(bearerToken, scenarioName, jasonTemplate,
					httpUrl, "POST", "Ideas", x);

			RestServicesUtil.gethttpResponseJasonVal2(con, "totalResults", x);

		} catch (Exception e) {
			throw new TollExceptions("error while sending request to create user " + e.getMessage());
		}
	}
	*/
	@Given("Get data From excel \"([^\"]*)\"")
	public void getData(String scenarioName) throws Exception {
		int columnNu =ExcelDrivenTest.findColumn(scenarioName,"Street_Address");
		System.out.println("column number is"+columnNu);
		String x =ExcelDrivenTest.getcellData(scenarioName,"User_Name");
		System.out.println(x);
		
		String y = ExcelDrivenTest.getcellData(scenarioName, "Street_Address");
		System.out.println(y);
	}
	
	
	

}
