package com.test.toll.glue;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.pages.IGIDashboard;
import com.test.toll.steps.IGIHomePageSteps;
import com.toll.exception.TollExceptions;

import cucumber.api.java.en.Given;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class ISIMServiceCentre {

	final Logger log = LoggerFactory.getLogger(ISIMServiceCentre.class);

	@Steps
	IGIHomePageSteps IGIHomePageSteps;
	IGIDashboard IGIDashboard;

	@Given("^user enters username and password for IGI")
	public void enterCredentials(){
		log.info("Entering user name and password");
		try {
			IGIHomePageSteps.enterCredentials();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			throw new TollExceptions("Error while entering user credentails" + e.getMessage());
		}

		//Check if succesully loged in or not
	}
	
	@Given("^Verify the error message")
	public void verifyLoginMessage(){
		IGIHomePageSteps.verifyInvalidLoginMessage();
	}
	 

	@Given("^user click on Logout")
	public void IGILogOut() throws Throwable {
		Thread.sleep(1000);
		//IGIHomePageSteps.logOut();
		Thread.sleep(1000);
	}

	@Given("^click on Configure tab")
	public void clickOnConfigure() throws Throwable {
		Thread.sleep(1000);
		IGIDashboard.tab_Configure.click();
			Thread.sleep(2000);
	}

	
	@Given("^server details should be displayed")
	public void checkServerDetails() throws Throwable {
	    	Thread.sleep(1000);
		String DbDetails = IGIDashboard.DbConfigurationDetails.getAttribute("innerText");
		Assert.assertTrue("Checking the connectons", DbDetails.trim().contains("Identity data store"));
		//Assert.assertEquals("Identity data store", DbDetails.trim());
	}
	
	
	 
	
	@Given("^Change Password")
	public void changePassword() throws Throwable {
		System.out.println("changing password");
		IGIHomePageSteps.changePassword();
		Thread.sleep(2000);

	}

}
