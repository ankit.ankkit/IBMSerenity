package com.test.toll.utilities;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.pages.ISAMHomePage;
import com.toll.exception.TollExceptions;

import ch.qos.logback.core.joran.action.Action;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;

public class GenericMethods extends PageObject {

	final Logger log = LoggerFactory.getLogger(GenericMethods.class);
	static GenericMethods genericMethods;
	public static void ConfigFileReader(List<String> propertyFilePaths) {
		InputStream input = null;
		Properties prop = new Properties();
		Iterator<String> propertyFilePath = propertyFilePaths.iterator();
		try {
			while (propertyFilePath.hasNext()) {
				input = new FileInputStream(propertyFilePath.next());
				try {
					prop.load(input);
				} catch (IOException e) {
					throw new TollExceptions("Properties file not found at " + propertyFilePath + e.getMessage());
				}
			}

			//System.out.println(prop.getProperty("PostUrl_CreateUser"));

		} catch (IOException e) {
			throw new TollExceptions("Properties file not found at " + propertyFilePath + e.getMessage());
		}
	}

	public static void loadPropertyFiles(String env) {
		List<String> propertyFilePaths = new ArrayList<String>();
		propertyFilePaths.add(System.getProperty("user.dir") + "/Configs/config.properties");
		propertyFilePaths.add(System.getProperty("user.dir") + "/Configs/" + env.toLowerCase() + ".properties");
		GenericMethods.ConfigFileReader(propertyFilePaths);

	}

	public static String getProperty(String key) throws IOException {
		try {
			File file = new File (System.getProperty("user.dir") + "/Configs/" + System.getProperty("env").toLowerCase() + ".properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			properties.getProperty(key);
			return properties.getProperty(key);
		}
		catch(NoSuchFileException e){
			throw new TollExceptions("Invalid path exception"+e.getMessage());
		}
		catch(Exception e){
				throw new TollExceptions("Error while getting value from properties file"+e.getMessage());
			}
	}
	
	//C:\GitLab_Ankit\IBMSerenity\SerenityTollDemoTest\src\main\APITemplates\ModifyUser.json"
	 public static String readJasonFile(String fileName)throws Exception
	  {
	    String data = "";
	    data = new String(Files.readAllBytes(Paths.get(fileName)));
	    return data;
	  }
	 
	 
		public static String createEncodedText(final String username,
				final String password) {
			final String pair = username + ":" + password;
			final byte[] encodedBytes = Base64.encodeBase64(pair.getBytes());
			return new String(encodedBytes);
		}

		public static void switchToNewWindow() {
			Set s = genericMethods.getDriver().getWindowHandles();
			Iterator itr = s.iterator();
			String w1 = (String) itr.next();
			String w2 = (String) itr.next();
			genericMethods.getDriver().switchTo().window(w2);
			}

			public static void switchToOldWindow() {
			Set s = genericMethods.getDriver().getWindowHandles();
			Iterator itr = s.iterator();
			String w1 = (String) itr.next();
			String w2 = (String) itr.next();
			genericMethods.getDriver().switchTo().window(w1);
			}

			public static void switchToParentWindow() {
				genericMethods.getDriver().switchTo().defaultContent();
			}


			/*
			* public static String getMethodName() {
			*
			* String methodName = Thread.currentThread().getStackTrace()[1]
			* .getMethodName(); System.out.println(methodName);
			*
			* return methodName; }
			*/

			public static void waitForElement(WebElement element) {

			WebDriverWait wait = new WebDriverWait(genericMethods.getDriver(), 10);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			}

			public static void waitTillElementFound(WebElement ElementTobeFound,
			int seconds) {
			WebDriverWait wait = new WebDriverWait(genericMethods.getDriver(), seconds);
			wait.until(ExpectedConditions.visibilityOf(ElementTobeFound));
			}

			public static void takeScreenshotOfWebelement(WebDriver driver,	WebElement element, String Destination) throws Exception {
			File v = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			BufferedImage bi = ImageIO.read(v);
			org.openqa.selenium.Point p = element.getLocation();
			int n = element.getSize().getWidth();
			int m = element.getSize().getHeight();
			BufferedImage d = bi.getSubimage(p.getX(), p.getY(), n, m);
			ImageIO.write(d, "png", v);

			FileUtils.copyFile(v, new File(Destination));
			}

			public static void takeScreenshotMethod(String Destination)
			throws Exception {
			File f = ((TakesScreenshot) genericMethods.getDriver()).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(f, new File(Destination));
			}

			public static void pressKeyDown(WebElement element) {
			element.sendKeys(Keys.DOWN);
			}

			public void pressKeyEnter(WebElement element) {
			element.sendKeys(Keys.ENTER);
			}

			public static void pressKeyUp(WebElement element) {
			element.sendKeys(Keys.UP);
			}

			public static void moveToTab(WebElement element) {
			element.sendKeys(Keys.chord(Keys.ALT, Keys.TAB));
			}

			public static void handleHTTPS_IEbrowser() {
				genericMethods.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");
			}


			public static void waitTillPageLoad(int i) {

				genericMethods.getDriver().manage().timeouts().pageLoadTimeout(i, TimeUnit.SECONDS);

			}

			public static void clickAllLinksInPage(String destinationOfScreenshot)
			throws Exception {

			List<WebElement> Links = genericMethods.getDriver().findElements(By.tagName("a"));
			System.out.println("Total number of links :" + Links.size());

			for (int p = 0; p < Links.size(); p++) {
			System.out.println("Elements present the body :"+ Links.get(p).getText());
			Links.get(p).click();
			Thread.sleep(3000);
			System.out.println("Url of the page " + p + ")"	+ genericMethods.getDriver().getCurrentUrl());
			takeScreenshotMethod(destinationOfScreenshot + p);
			navigate_back();
			Thread.sleep(2000);
			}

			}

			public static void keyboardEvents(WebElement webelement, Keys key,
			String alphabet) {
			webelement.sendKeys(Keys.chord(key, alphabet));

			}

			public static void navigate_forward() {
				genericMethods.getDriver().navigate().forward();
			}

			public static void navigate_back() {
				genericMethods.getDriver().navigate().back();
			}

			public static void refresh() {
				genericMethods.getDriver().navigate().refresh();
			}

			public static void waitMyTime(int i) {
				genericMethods.getDriver().manage().timeouts().implicitlyWait(i, TimeUnit.SECONDS);

			}

			public static void clearTextField(WebElement element) {
			element.clear();

			}

			public static void clickWebelement(WebElement element) {
			try {
			boolean elementIsClickable = element.isEnabled();
			while (elementIsClickable) {
			element.click();
			}

			} catch (Exception e) {
			System.out.println("Element is not enabled");
			e.printStackTrace();
			}
			}

			public static void clickMultipleElements(WebElement someElement,
			WebElement someOtherElement) {
			Actions builder = new Actions(genericMethods.getDriver());
			builder.keyDown(Keys.CONTROL).click(someElement)
			.click(someOtherElement).keyUp(Keys.CONTROL).build().perform();
			}

			public static void highlightelement(WebElement element) {
			for (int i = 0; i < 4; i++) {
			JavascriptExecutor js = (JavascriptExecutor) genericMethods.getDriver();
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);",element, "color: solid red; border: 6px solid yellow;");
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);",element, "");

			}

			}

			public static boolean checkAlert_Accept() {
			try {
			Alert a = genericMethods.getDriver().switchTo().alert();
			String str = a.getText();
			System.out.println(str);

			a.accept();
			return true;

			} catch (Exception e) {

			System.out.println("no alert ");
			return false;

			}
			}

			public static boolean checkAlert_Dismiss() {
			try {
			Alert a = genericMethods.getDriver().switchTo().alert();
			String str = a.getText();
			System.out.println(str);

			a.dismiss();
			return true;

			} catch (Exception e) {

			System.out.println("no alert ");
			return false;

			}
			}

/*			public static void scrolltoElement(WebElement ScrolltoThisElement) {
			Coordinates coordinate = ((Locatable) ScrolltoThisElement)
			.getCoordinates();
			coordinate.onPage();
			coordinate.inViewPort();
			}*/

			public static void checkbox_Checking(WebElement checkbox) {
			boolean checkstatus;
			checkstatus = checkbox.isSelected();
			if (checkstatus == true) {
			System.out.println("Checkbox is already checked");
			} else {
			checkbox.click();
			System.out.println("Checked the checkbox");
			}
			}

			public static void radiobutton_Select(WebElement Radio) {
			boolean checkstatus;
			checkstatus = Radio.isSelected();
			if (checkstatus == true) {
			System.out.println("RadioButton is already checked");
			} else {
			Radio.click();
			System.out.println("Selected the Radiobutton");
			}
			}

			// Unchecking
			public static void checkbox_Unchecking(WebElement checkbox) {
			boolean checkstatus;
			checkstatus = checkbox.isSelected();
			if (checkstatus == true) {
			checkbox.click();
			System.out.println("Checkbox is unchecked");
			} else {
			System.out.println("Checkbox is already unchecked");
			}
			}

			public static void radioButton_Deselect(WebElement Radio) {
			boolean checkstatus;
			checkstatus = Radio.isSelected();
			if (checkstatus == true) {
			Radio.click();
			System.out.println("Radio Button is deselected");
			} else {
			System.out.println("Radio Button was already Deselected");
			}
			}

			public static void dragAndDrop(WebElement fromWebElement,
			WebElement toWebElement) {
			Actions builder = new Actions(genericMethods.getDriver());
			builder.dragAndDrop(fromWebElement, toWebElement);
			}

/*			public static void dragAndDrop_Method2(WebElement fromWebElement,
			WebElement toWebElement) {
			Actions builder = new Actions(genericMethods.getDriver());
			Action dragAndDrop = builder.clickAndHold(fromWebElement).moveToElement(toWebElement).release(toWebElement).build();
			dragAndDrop.perform();
			}*/

			public static void dragAndDrop_Method3(WebElement fromWebElement,
			WebElement toWebElement) throws InterruptedException {
			Actions builder = new Actions(genericMethods.getDriver());
			builder.clickAndHold(fromWebElement).moveToElement(toWebElement)
			.perform();
			Thread.sleep(2000);
			builder.release(toWebElement).build().perform();
			}

			public static void hoverWebelement(WebElement HovertoWebElement)
			throws InterruptedException {
			Actions builder = new Actions(genericMethods.getDriver());
			builder.moveToElement(HovertoWebElement).perform();
			Thread.sleep(2000);

			}

			public static void doubleClickWebelement(WebElement doubleclickonWebElement)
			throws InterruptedException {
			Actions builder = new Actions(genericMethods.getDriver());
			builder.doubleClick(doubleclickonWebElement).perform();
			Thread.sleep(2000);

			}

			public static String getToolTip(WebElement toolTipofWebElement)
			throws InterruptedException {
			String tooltip = toolTipofWebElement.getAttribute("title");
			System.out.println("Tool text : " + tooltip);
			return tooltip;
			}

			public static void selectElementByNameMethod(WebElement element, String Name) {
			Select selectitem = new Select(element);
			selectitem.selectByVisibleText(Name);
			}

			public static void selectElementByValueMethod(WebElement element,
			String value) {
			Select selectitem = new Select(element);
			selectitem.selectByValue(value);
			}

			public static void selectElementByIndexMethod(WebElement element, int index) {
			Select selectitem = new Select(element);
			selectitem.selectByIndex(index);
			}

			public static void clickCheckboxFromList(String xpathOfElement,
			String valueToSelect) {

			List<WebElement> lst = genericMethods.getDriver().findElements(By.xpath(xpathOfElement));
			for (int i = 0; i < lst.size(); i++) {
			List<WebElement> dr = lst.get(i).findElements(By.tagName("label"));
			for (WebElement f : dr) {
			System.out.println("value in the list : " + f.getText());
			if (valueToSelect.equals(f.getText())) {
			f.click();
			break;
			}
			}
			}
			}

			public static void downloadFile(String href, String fileName)
			throws Exception {
			URL url = null;
			URLConnection con = null;
			int i;
			url = new URL(href);
			con = url.openConnection();
			File file = new File(".//OutputData//" + fileName);
			BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
			BufferedOutputStream bos = new BufferedOutputStream(
			new FileOutputStream(file));
			while ((i = bis.read()) != -1) {
			bos.write(i);
			}
			bos.flush();
			bis.close();
			}
/*
		public static void click(WebElement element) {
			genericMethods.getDriver().element.click();
		}
*/
}

