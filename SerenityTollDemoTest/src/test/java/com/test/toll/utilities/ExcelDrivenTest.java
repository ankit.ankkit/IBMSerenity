package com.test.toll.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csvreader.CsvReader;
import com.toll.exception.TollExceptions;

import junit.framework.Assert;

public class ExcelDrivenTest {

	static final Hashtable DICT_TO_READ = new Hashtable();
	static Logger log = LoggerFactory.getLogger(ExcelDrivenTest.class);

	public static String getData(String t_testcaseName, String t_field, int t_instance) {
		try {
			if (DICT_TO_READ.isEmpty() == false) {
				DICT_TO_READ.clear();
			}
			int flag = 0;
			String testDataPath = "src/test/resources/iDev2InputDataSheet.csv";
			CsvReader csvReaderObj = new CsvReader(testDataPath);
			csvReaderObj.readHeaders();
			while (csvReaderObj.readRecord()) {
				String p_testcaseName = csvReaderObj.get("TestcaseName").trim();
				String p_testcaseInstance = csvReaderObj.get("TestcaseInstance").trim();
				if ((t_testcaseName.equalsIgnoreCase(p_testcaseName))
						&& (t_instance == Integer.parseInt(p_testcaseInstance))) {
					for (int i = 1; i < csvReaderObj.getColumnCount() / 2 + 1; i++) {
						String p_field = csvReaderObj.get("Field" + i).trim();
						String p_objproperty = csvReaderObj.get("Value" + i).trim();
						if (p_field.equalsIgnoreCase(t_field)) {
							DICT_TO_READ.put(p_field, p_objproperty);
							break;
						}
					}
					flag = 0;
					break;
				} else {
					flag = 1;
				}
			}
			if (flag == 1) {
				Assert.fail("No data present for testname " + t_testcaseName);
			}
		} catch (FileNotFoundException e) {
			Assert.fail("Test data file not found");
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
		return (String) DICT_TO_READ.get(t_field);
	}

	public static String driveData(String testScenarioName, String field) {
		System.out.println(field);
		String data = getData(testScenarioName, field, 1);
		return data;
	}

	public static int findRow(Sheet sheet, String cellContent) {
		for (Row row : sheet) {
			for (Cell cell : row) {
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
						return row.getRowNum();
					}
				}
			}
		}
		return 0;
	}

	public static int findColumn(String testScenarioName, String t_field) throws Exception {
		String excelFilePath = GenericMethods.getProperty("testDataPath");
		HashMap<String, String> testDataHashMap = new HashMap<String, String>();

		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Row r = firstSheet.getRow(0);

		int patchColumn = -1;
		for (int cn = 0; cn < r.getLastCellNum(); cn++) {
			Cell c = r.getCell(cn);
			if (c == null || c.getCellType() == Cell.CELL_TYPE_BLANK) {
				// Can't be this cell - it's empty
				continue;
			}
			if (c.getCellType() == Cell.CELL_TYPE_STRING) {
				String text = c.getStringCellValue();
				if (t_field.equals(text)) {
					patchColumn = cn;
					break;
				}
			}
		}
		return patchColumn;
	}

	@SuppressWarnings("deprecation")
	public static HashMap<String, String> getTestDataFromExcel(String testScenarioName){

		try {
			String excelFilePath = GenericMethods.getProperty("testDataPath");
			HashMap<String, String> testDataHashMap = new HashMap<String, String>();
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			int noOfColumns = firstSheet.getRow(0).getLastCellNum();
			int rownr = ExcelDrivenTest.findRow(firstSheet, testScenarioName);
	
			for (int i = 2; i < noOfColumns; i++) {
				Cell Value = firstSheet.getRow(rownr).getCell(i);
				if (Value == null) {
					testDataHashMap.put(firstSheet.getRow(0).getCell(i).toString(), "Empty");
				} else {
					testDataHashMap.put("&" + firstSheet.getRow(0).getCell(i).toString(), Value.toString());
				}
			}
			workbook.close();
			inputStream.close();
			return testDataHashMap;
		}
		catch(Exception e){
			throw new  TollExceptions("Exception while getTestDataFromExcel" + e.getMessage());
		}
	}

	@SuppressWarnings("deprecation")
	public static String getcellData(String testScenarioName, String t_field){
		String excelFilePath;
		try {
			excelFilePath = GenericMethods.getProperty("testDataPath");
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			int noOfColumns = firstSheet.getRow(0).getLastCellNum();
			int rownr = ExcelDrivenTest.findRow(firstSheet, testScenarioName);
			int colnr = ExcelDrivenTest.findColumn(testScenarioName, t_field);
			String cellValue = firstSheet.getRow(rownr).getCell(colnr).getStringCellValue();
			workbook.close();
			inputStream.close();
			return cellValue;
	}
		catch (Exception e) {
			throw new  TollExceptions("Exception while getting cell data" + e.getMessage());

		}
	}
}
