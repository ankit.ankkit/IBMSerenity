package com.test.toll.utilities;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.google.inject.internal.Annotations;

import net.serenitybdd.core.annotations.findby.By;

public class UIInteraction {

	/*
	 * public static void MouseOver(WebElement we,Driver driver){ Actions actObj=new
	 * Actions(driver); actObj.moveToElement(we).build().perform(); }
	 */
	
	/*public static WebDriver OpenApp(String BrowserName, String url){
		fn_LaunchBrowser(BrowserName);
		fn_OpenURL(url);
		return driver;
		}

	public static void fn_OpenURL(String url){
		driver.get(url);
		driver.manage().window().maximize();
		}
		 
		public static WebDriver fn_LaunchBrowser(String browsername){
		if(browsername=="CH"){
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		driver= new ChromeDriver();
		}else if(browsername=="FF"){
		driver= new FirefoxDriver();
		}else if(browsername=="IE"){
		System.setProperty("webdriver.ie.driver", "Drivers\\IEDriverServer.exe");
		driver= new InternetExplorerDriver();
		}
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		return driver;
		}*/
	
	
	public static String fn_TakeSnapshot(WebDriver driver, String DestFilePath) throws IOException {
		String TS = fn_GetTimeStamp();
		TakesScreenshot tss = (TakesScreenshot) driver;
		File srcfileObj = tss.getScreenshotAs(OutputType.FILE);
		DestFilePath = DestFilePath + TS + ".png";
		File DestFileObj = new File(DestFilePath);
		FileUtils.copyFile(srcfileObj, DestFileObj);
		return DestFilePath;
	}

	public static String fn_GetTimeStamp() {
		DateFormat DF = DateFormat.getDateTimeInstance();
		Date dte = new Date();
		String DateValue = DF.format(dte);
		DateValue = DateValue.replaceAll(":", "_");
		DateValue = DateValue.replaceAll(",", "");
		return DateValue;
	}

	// select the dropdown using "select by visible text", so pass VisibleText as
	// 'Yellow' to funtion
	public static void fn_SelectByText(WebElement WE, String VisibleText) {
		Select selObj = new Select(WE);
		selObj.selectByVisibleText(VisibleText);
	}

	// select the dropdown using "select by index", so pass IndexValue as '2'
	public static void fn_Select(WebElement WE, int IndexValue) {
		Select selObj = new Select(WE);
		selObj.selectByIndex(IndexValue);
	}

	// select the dropdown using "select by value", so pass Value as 'thirdcolor'
	public static void fn_Select(WebElement WE, String Value) {
		Select selObj = new Select(WE);
		selObj.selectByValue(Value);
	}

	public static void Select_The_Checkbox(WebElement element) {
		try {
			if (element.isSelected()) {
				System.out.println("Checkbox: " + element + "is already selected");
			} else {
				// Select the checkbox
				element.click();
			}
		} catch (Exception e) {
			System.out.println("Unable to select the checkbox: " + element);
		}
	}

	public static void DeSelectTheCheckbox(WebElement element) {
		try {
			if (element.isSelected()) {
				// De-select the checkbox
				element.click();
			} else {
				System.out.println("Checkbox: " + element + "is already deselected");
			}
		} catch (Exception e) {
			System.out.println("Unable to deselect checkbox: " + element);
		}
	}

	public void Select_The_CheckBox_from_List(WebElement element, String valueToSelect) {
		List<WebElement> allOptions = element.findElements(By.tagName("input"));
		for (WebElement option : allOptions) {
			System.out.println("Option value " + option.getText());
			if (valueToSelect.equals(option.getText())) {
				option.click();
				break;
			}
		}
	}

	public static List<WebElement> getElementsByTagName(WebElement element, String optionName)
    {
       List<WebElement> listOfElements = element.findElements(By.tagName(optionName));
       if(listOfElements.size()!=0)
          return listOfElements;
       else
    	   return null;
    }
	
	public static String get_SelectedOption(WebElement element) {
		Select select = new Select(element);
		WebElement selectedElement = select.getFirstSelectedOption();
		String selectedOption = selectedElement.getText();
		return selectedOption;
	}
	 public static void select_Option_In_DropDown_ByVisibleText(
	            WebElement element, String sVisibleTextOptionToSelect) {
	        try {
	            Select select = new Select(element);
	            select.selectByVisibleText(sVisibleTextOptionToSelect);
	           
	        } catch (NoSuchElementException e) {
	            System.out.println("Option value not find in dropdown");
	        
	        }
	    }
	 public static boolean select_Option_In_DropDown_ByIndexVal(WebElement element, String sValueTextToSelect) {
	        try {
	            Select select = new Select(element);
	            select.selectByValue("nokia");
	            return true;
	        } catch (NoSuchElementException e) {
	            System.out.println("Value not find in dropdown");
	            return false;
	          }
	    }

	 public static void select_Option_In_DropDown_ByValue(WebElement element, int indexVal) {
	        try {
	            Select select = new Select(element);
	            select.selectByIndex(indexVal);
	        } catch (NoSuchElementException e) {
	            System.out.println("Option value not find in dropdown");
	        }
	    }
	 public static boolean verify_Values_In_Dropdown(List<WebElement> listOfElements, String[] strValues) {
			boolean bValue=false;
			List<String> list = new ArrayList<String>();
			for (String strValue : strValues) {
				boolean bflag = false;
				for (WebElement element : listOfElements) {
					String elementValue = element.getText();
					if (strValue.equals(elementValue)) {
						bflag= true;
					}
				}
				if (!bflag)
					list.add(strValue);
			}
			if (list.size() > 0) {
				for(String strList : list) {
					System.out.println("Value not present in dropdown: "+strList);
				}
				//Assign false if any of the value not found in dropdown
				bValue = false;
			} else {
				//Assign true if all values found in dropdown
				System.out.println("All value(s) found in dropdown");
				bValue=true;
			}
			return bValue;
		}
	 
	 //Below method is used to get the main window handle.
	 public static String getMainWindowHandle(WebDriver driver) {
			return driver.getWindowHandle();
		}
	 
	 //Below method is used to get the current window title
		public static String getCurrentWindowTitle(WebDriver driver) {
			String windowTitle = driver.getTitle();
			return windowTitle;
		}
		//Below method is used to close all the other windows except the main window
		public static boolean closeAllOtherWindows(WebDriver driver, String openWindowHandle) {
			Set<String> allWindowHandles = driver.getWindowHandles();
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(openWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);
					driver.close();
				}
			}
			
			driver.switchTo().window(openWindowHandle);
			if (driver.getWindowHandles().size() == 1)
				return true;
			else
				return false;
		}
		
		//Below method is used to wait for the new window to be present and switch to it.

		 public static void waitForNewWindowAndSwitchToIt(WebDriver driver) throws InterruptedException {
		        String cHandle = driver.getWindowHandle();
		        String newWindowHandle = null;
		        Set<String> allWindowHandles = driver.getWindowHandles();
		        
		        //Wait for 20 seconds for the new window and throw exception if not found
		        for (int i = 0; i < 20; i++) {
		            if (allWindowHandles.size() > 1) {
		                for (String allHandlers : allWindowHandles) {
		                    if (!allHandlers.equals(cHandle))
		                    	newWindowHandle = allHandlers;
		                }
		                driver.switchTo().window(newWindowHandle);
		                break;
		            } else {
		                Thread.sleep(1000);
		            }
		        }
		        if (cHandle == newWindowHandle) {
		            throw new RuntimeException(
		                    "Time out - No window found");
		        }
		    }
		 
/*
		 public static By getBy(Class cls,String fieldName) {
			 try {
				 System.out.println(cls.getDeclaredField(fieldName));
				 return new Annotations(cls.getDeclaredField(fieldName)).buildBy();
			 }
		 catch(NoSuchFieldException) {
			 return null;
		 }
		 }*/
		 
	}
		

