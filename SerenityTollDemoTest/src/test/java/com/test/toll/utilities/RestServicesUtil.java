package com.test.toll.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.toll.exception.TollExceptions;

import net.serenitybdd.core.Serenity;

public class RestServicesUtil {

	final static Logger log = LoggerFactory.getLogger(RestServicesUtil.class);
	/**
	 * This method can be used for various HTTP response like Post,PUT
	 * 
	 * @author ankit garg
	 * @preCondition :This will get bearrer token with admin,admin,admin
	 * @param scenarioName
	 *            : This is passed from features file
	 * @param jasonTemplate
	 *            : Template for Body.Values for this will be replaced from excel
	 *            file. Path is defined in Properties file
	 * @param httpUrl
	 *            : Url to send the http request
	 * @param requestType
	 *            : POST,PUT
	 * @param :
	 *            realmVal : Admin /Ideas
	 * @param actualRespCode
	 *            : Reponse code expected from the http url
	 * @throws IOException
	 */

	public static HttpURLConnection sendHttpRequestAndValidateResponseCode(String autToken, String scenarioName,
			String jasonTemplate, String httpUrl, String requestType, String realmVal, int resCode) throws Exception {

		log.info("Inside method sendHttpRequestAndValidateResponseCode");
		Serenity.setSessionVariable("scenarioName").to(scenarioName);
		log.info("Reading jason params and values from excel");

		HashMap<String, String> jsonParams = ExcelDrivenTest.getTestDataFromExcel(scenarioName);

		String payloadTemplate = GenericMethods
				.readJasonFile(GenericMethods.getProperty("jSonTemplatePath") + jasonTemplate + ".json");

		for (Entry<String, String> paramEntry : jsonParams.entrySet()) {
			payloadTemplate = payloadTemplate.replaceAll(paramEntry.getKey(), paramEntry.getValue());
		}
		System.out.println("**********Final Post request is ***********");
		System.out.println(payloadTemplate);
		System.out.println("*********************************************");
		HttpURLConnection con = executehttpMethod(httpUrl, autToken, payloadTemplate, requestType, realmVal);
		log.info("Response message for scenario" + scenarioName + "is :" + con.getResponseMessage());
		int actualRespCode = con.getResponseCode();

		log.info("Response Code for scenario " + scenarioName + "is :" + actualRespCode);
		Assert.assertTrue(actualRespCode == resCode);
		return con;

	}

	private static HttpURLConnection executehttpMethod(String postUrl, String BearerToken, String payloadTemplate,
			String requestType, String realmVal) {
		URL obj = null;
		OutputStream os = null;
		HttpURLConnection con = null;
		try {
			System.out.println("Post url is " + postUrl);
			obj = new URL(postUrl);
		} catch (IOException e) {
			throw new TollExceptions("Exception in url" + e.getMessage());
		}

		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		con.setDoOutput(true);
		try {
			con.setRequestMethod(requestType);
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		con.setRequestProperty("Content-Type", "application/scim+json");
		con.setRequestProperty("Accept", "application/scim+json");
		con.setRequestProperty("realm", realmVal);
		con.setRequestProperty("Authorization", "Bearer " + BearerToken);

		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			os.write(payloadTemplate.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	public static String getBearerToken(String realmValue) throws IOException {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc;
			sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			new TollExceptions("Exception in SSL Con while generating the bearer token" + e.getMessage());
		}

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		String userName = GenericMethods.getProperty("userNameToken");
		String pass = GenericMethods.getProperty("passforToken");

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		URL url = new URL(GenericMethods.getProperty("baseUrl") + GenericMethods.getProperty("getBearerTokenAPIPath"));

		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		String originalInput = userName + ':' + pass;
		String encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());

		con.setRequestProperty("Authorization", "Basic " + encodedString);
		con.setRequestProperty("realm", realmValue);

		int responseCode = con.getResponseCode();
		log.info("Response code from Get bearer token :: " + responseCode);

		Assert.assertTrue(responseCode == 200);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		} else {
			log.info("GET request not worked");
			return null;
		}

	}

	/**
	 * @param con
	 * @param actualRespCode
	 * @throws IOException
	 */
	public static String gethttpResponseJasonVal(HttpURLConnection con, String jsonObject, int actualRespCode)
			throws IOException {
		if (actualRespCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			System.out.println(in);
			String JsonString = "";
			for (int c; (c = in.read()) >= 0;) {
				JsonString += (char) c;
			}
			JSONObject JSON = new JSONObject(JsonString);
			return (String) JSON.get(jsonObject);
		} else {
			return null;
		}
	}

	
	
	/**
	 * @param con
	 * @param actualRespCode
	 * @throws IOException
	 */
	public static void gethttpResponseJasonVal2(HttpURLConnection con, String jsonObject, int actualRespCode)
			throws IOException {
		if (actualRespCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String JsonString = "";
			for (int c; (c = in.read()) >= 0;) {
				JsonString += (char) c;
			}
			System.out.println(JsonString);
			JSONObject JSON = new JSONObject(JsonString);
			JSONArray arr= JSON.getJSONArray("resources");
			System.out.println(arr.length());
		} else {
			//return null;
		}
	}
	/**
	 * @param con
	 * @param actualRespCode
	 * @throws IOException
	 */
	public static int gethttpResponseCode(HttpURLConnection con, int actualRespCode) throws IOException {
		return con.getResponseCode();
	}

}
