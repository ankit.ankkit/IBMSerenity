/*package com.test.toll.utilities;

import java.awt.Point;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.serenitybdd.core.annotations.findby.By;

public class WebElements {

	final Logger log = LoggerFactory.getLogger(WebElements.class);
              private By by;
              private String name;
              private WebElement element;

              public WebElements(String name, By by) {
                             this.by = by;
                             this.name = name;
              }

              public void verifyDisplayed() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           log.info("PASSED", name + " Element is displayed", name + " Element is displayed");
                             }

              }

              public void verifyNotDisplayed() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("PASSED", name + " Element is NOT displayed", name + " Element is NOT displayed");
                             } else {
                                           log.info("FAILED", name + " Element is displayed", name + " Element is NOT displayed");
                             }

              }

              public void verifyEnabled() {

                             element = Browser.findElement(by);

                             if (element.isEnabled()) {
                                           log.info("PASSED", name + " Element is enabled", name + " Element is enabled");
                             } else {
                                           log.info("FAILED", name + " Element is disabled", name + " Element is enabled");
                             }

              }

          public void verifyDisabled() {

                             element = Browser.findElement(by);

                             if (element.isEnabled()) {
                                           log.info("FAILED", name + " Element is enabled", name + " Element is disabled");
                             } 
                             else {
                                           log.info("PASSED", name + " Element is disabled", name + " Element is disabled");
                             }
              }
              
              public Boolean verifyDisabled() {

                             element = Browser.findElement(by);

                             if (element.isEnabled()) {
                                           log.info("FAILED", name + " Element is enabled", name + " Element is disabled");
                                           return false;
                             } 
                             else {
                                           log.info("PASSED", name + " Element is disabled", name + " Element is disabled");
                                           return true;
                             }
              }
              
              public void doubleClick() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           Actions action = new Actions(Browser.getDriver());
                                           action.moveToElement(element);
                                           action.doubleClick();
                                           action.perform();
                                           log.info("PASSED", name + " Element is double clicked", name + " Element is double clicked");
                             }
              }

              public void setText(String text) {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           element.clear();
                                           element.sendKeys(text);
                                           log.info("PASSED", name + " Element is populated with '" + text + "'",
                                                                        name + " Element is populated with '" + text + "'");
                             }
              }

              public void setTextEnterKey() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           element.sendKeys(Keys.ENTER);
                                           log.info("PASSED", name + " Enter Key is pressed ", name + " Enter Key is entered ");
                             }
              }

              public void sendKeys(String text) {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           element.sendKeys(text);
                                           log.info("PASSED", name + " Element is populated with '" + text + "'",
                                                                        name + " Element is populated with '" + text + "'");
                             }
              }

              public void setTextArrowDownKey() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           element.sendKeys(Keys.DOWN);
                                           log.info("PASSED", name + " Arrow Down Key is pressed ", name + " Arrow Down Key is entered ");
                             }
              }
              
              public static boolean isElementPresent(String xpath){
                             try{
                                           
                                           Browser.findElement(By.xpath(xpath));
                                           
                                           return true;
                             }
                             
                             catch(Throwable T)
                             {
                                           return false;
                             }
                             
              }
              
              
              
              
              
              

              public void setTextArrowRightKey() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           element.sendKeys(Keys.RIGHT);
                                           log.info("PASSED", name + " Arrow RIGHT Key is pressed ", name + " Arrow RIGHT Key is entered ");
                             }
              }

              public void hoverElement() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           Actions builder = new Actions(Browser.getDriver());
                                           builder.moveToElement(element).build().perform();
                                           log.info("PASSED", "Hover to " + name, "Hover to " + name);
                             }
              }

              public void hoverElementJS() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
                                           ((JavascriptExecutor) Browser.getDriver()).executeScript(mouseOverScript, element);
                                           log.info("PASSED", "Hover to " + name, "Hover to " + name);
                             }
              }

              public void click() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           try {
                                                          element.click();
                                           } catch (Exception e) {
                                                          try {
                                                                        Thread.sleep(5000);
                                                          } catch (InterruptedException e1) {
                                                          }
                                                          JavascriptExecutor executor = (JavascriptExecutor) Browser.getDriver();
                                                         executor.executeScript("arguments[0].click();", element);
                                           }

                                           log.info("PASSED", name + " Element is clicked", name + " Element is clicked");
                             }
              }

              public void rightClick() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           Actions action = new Actions(Browser.getDriver()).contextClick(element);
                                           action.build().perform();
                                           log.info("PASSED", name + " Element is right clicked", name + " Element is clicked");
                             }
              }

              public String getValue() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                                           return "";
                             } else {
                                           return element.getAttribute("value");
                             }
              }

              public void submit() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           log.info("PASSED", name + " Element is submitted", name + " Element is submitted");
                                           element.submit();
                             }

              }

              public void sendKeys(CharSequence... keysToSend) {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           log.info("PASSED", name + " Element is populated with " + keysToSend,
                                                                        name + " Element is populated with " + keysToSend);
                                           element.sendKeys(keysToSend);
                             }

              }

              public void clear() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           log.info("PASSED", name + " Element is cleared ", name + " Element is cleared");
                                           element.clear();
                             }
              }

              public String getTagName() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                                           return null;
                             } else {
                                           log.info("PASSED", name + " Element TagName  is " + element.getTagName(),
                                                                        name + " Element TagName  is " + element.getTagName());
                                           return element.getTagName();
                             }
              }

              public String getAttribute(String aname) {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                                           return null;
                             } else {
                                           log.info("PASSED", name + " Element '" + name + "' Attribute  is " + element.getAttribute(aname),
                                                                        name + " Element '" + name + "' Attribute  is " + element.getAttribute(aname));
                                           return element.getAttribute(aname);
                             }
              }

              public boolean isSelected() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           return false;
                             } else {
                                           return element.isSelected();
                             }
              }

              public boolean isEnabled() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           return false;
                             } else {
                                           return element.isEnabled();
                             }
              }

              public String getText() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           // log.info("FAILED", name+" Element is NOT displayed", name+"
                                           // Element is displayed");
                                           return null;
                             } else {
                                           // log.info("PASSED", name+" Element text is
                                           // "+element.getText(), name+" Element text is "+element.getText());
                                           return element.getText();
                             }
              }

              public String getCssValue(String propertyName) {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           return null;
                             } else {
                                           return element.getCssValue(propertyName);
                             }
              }

              public boolean isDisplayed() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           return false;
                             } else {
                                           return element.isDisplayed();
                             }
              }

              public Integer getYCoordinate() {
                             element = Browser.findElement(by);

                             Point coordinates = element.getLocation();

                             if (element == null) {
                                           return 0;
                             } else {
                                           return coordinates.getY();
                             }
              }

              public Integer getXCoordinate() {
                             element = Browser.findElement(by);

                             Point coordinates = element.getLocation();

                             if (element == null) {
                                           return 0;
                             } else {
                                           return coordinates.getX();
                             }
              }

              public Integer getHeight() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           return 0;
                             } else {
                                           return element.getSize().height;
                             }
              }

              public Integer getWidth() {
                             element = Browser.findElement(by);

                             if (element == null) {
                                           return 0;
                             } else {
                                           return element.getSize().width;
                             }
              }

              public void verifyDisplayedBySize() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else if (element.getSize().height == 0 | element.getSize().width == 0) {
                                           log.info("FAILED", name + " Element is NOT displayed", name + " Element is displayed");
                             } else {
                                           log.info("PASSED", name + " Element is displayed", name + " Element is displayed");
                             }

              }

              public void verifyNOTDisplayedBySize() {

                             element = Browser.findElement(by);

                             if (element == null) {
                                           log.info("PASSED", name + " Element is NOT displayed", name + " Element is NOT displayed");
                             } else if (element.getSize().height == 0 | element.getSize().width == 0) {
                                           log.info("PASSED", name + " Element is NOT displayed", name + " Element is NOT displayed");
                             } else {
                                           log.info("FAILED", name + " Element is displayed", name + " Element is NOT displayed");
                             }

              }
              
              

}
*/