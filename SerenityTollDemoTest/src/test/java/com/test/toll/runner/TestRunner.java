package com.test.toll.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

/**
 *Created by ankit.garg on 6/06/2018
 */

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(monochrome = true, 
					features = "src/test/resources", 
					glue = "com.test.toll.glue", 
					tags="@Duplicateemail",
					format = {
					"pretty", "html:target/cucumber-reports", "json:target/cucumber-reports/cucumber.json"})

public class TestRunner {

}