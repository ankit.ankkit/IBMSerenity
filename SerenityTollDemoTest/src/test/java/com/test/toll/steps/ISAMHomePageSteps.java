package com.test.toll.steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.toll.glue.ISAMBasicGlue;
import com.test.toll.pages.ISAMHomePage;
import com.test.toll.pages.SecurityAccessManager;
import com.test.toll.utilities.ExcelDrivenTest;

import junit.framework.Assert;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.Step;

public class ISAMHomePageSteps {

	ISAMHomePage IsamHomePage;
	SecurityAccessManager SamHomePage;
	final Logger slf4jLogger = LoggerFactory.getLogger(ISAMHomePageSteps.class);

	@Step
	public void enterCredentials() throws InterruptedException {
		Serenity.sessionVariableCalled("scenarioName").toString();
		String username = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"User_Name");
		String password = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"Password");

		if (IsamHomePage.loginName.isCurrentlyVisible()) {
			IsamHomePage.login(username, password);
		} else {
			SamHomePage.login(username, password);
		}
	}

	@Step
	public void logout(String logoutUrl) throws InterruptedException {
		IsamHomePage.getDriver().navigate().to(logoutUrl);
		Assert.assertTrue(SamHomePage.verifyingLogoutPage.isDisplayed());

	}

	@Step
	public void navigateToUrl(String url) throws InterruptedException {
		IsamHomePage.getDriver().navigate().to(url);
	}

	@Step
	public void verifyHomePage() {
		String result = ExcelDrivenTest.driveData(Serenity.sessionVariableCalled("scenarioName").toString(), "Result");
		String scenario = Serenity.sessionVariableCalled("scenarioName").toString();

		if (IsamHomePage.loginName.isCurrentlyVisible()) {
			if (scenario.contains("positive")) {
				Assert.assertTrue(SamHomePage.homePage.isDisplayed());
			} else if (scenario.contains("negative")) {
				Assert.assertTrue(SamHomePage.errorMsg.isDisplayed());

			}

		} else {
			if (scenario.contains("positive")) {
				IsamHomePage.productName.getText().contentEquals("IBM Security Access Manager");
			} else if (scenario.contains("negative")) {
				Assert.assertTrue("Verifying the invalid login attempt", IsamHomePage.invalidLoginErrorMsg.isPresent());
			}

		}

	}

	@Step
	public void verifyLoginPage() {
		Assert.assertTrue(IsamHomePage.loginName.isCurrentlyVisible());

	}

	@Step
	public void verifydefaultTabs() {

		System.out.println(IsamHomePage.tabMenuItems.size());
	}

	@Step
	public void ClickLogoutButton() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(IsamHomePage.getDriver(), 40);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//span[@id='btn-user']//span[contains(@class,'dijitDownArrowButton')]")));
		IsamHomePage.adminDropDownButton.click();
		Thread.sleep(1000);
		IsamHomePage.logoutButton.click();
		Thread.sleep(2000);
	}

	@Step
	public void VerifySession() {
		System.out.println("Verify session out (home page)");
	}

	public void verifyHeaders() {
		IsamHomePage.getDriver().findElement(By.xpath("//td[contains(.,'iv-creds:')]")).isDisplayed();
		IsamHomePage.getDriver().findElement(By.xpath("//td[contains(.,'iv-groups:')]")).isDisplayed();
		IsamHomePage.getDriver().findElement(By.xpath("//td[contains(.,'iv-remote-address:')]")).isDisplayed();
	}

	
	//******************MAy be removed
	@Step
	public void enterCredentials2() throws InterruptedException {

		String x = Serenity.sessionVariableCalled("scenarioName").toString();
		String username = ExcelDrivenTest.driveData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"Username");
		String password = ExcelDrivenTest.driveData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"Password");

		if (IsamHomePage.loginName.isCurrentlyVisible()) {
			IsamHomePage.login(username, password);
		} else {
			SamHomePage.login(username, password);
		}
	}

}
