package com.test.toll.steps;

import org.junit.Assert;

import com.test.toll.pages.IGIServiceCentre;
import com.test.toll.utilities.ExcelDrivenTest;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class IGIHomePageSteps {

	IGIServiceCentre IGISrviceCentre;

	@Step
	public void enterCredentials() throws InterruptedException {

		String username = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"User_Name");
		String password = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"Password");
		IGISrviceCentre.login(username, password);

	}

	public void changePassword() throws InterruptedException {

		System.out.println("Inside change");
		IGISrviceCentre.HeaderImages.get(3).click();
		IGISrviceCentre.changePassword.get(0).click();
		Thread.sleep(2000);
		IGISrviceCentre.selectUserForPasswordChange.click();
		IGISrviceCentre.changePasButon.click();

	}

	public void logOut() throws InterruptedException {
		IGISrviceCentre.cancelAndLogoutButton.click();
	}

	public void verifyInvalidLoginMessage() {
		Assert.assertTrue("Verifying message", IGISrviceCentre.verifyMessage.getAttribute("innerText").contains("Invalid credentials"));

	}

}
