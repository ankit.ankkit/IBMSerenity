package com.test.toll.steps;

import com.test.toll.pages.IGIAdminConsole;
import com.test.toll.pages.IGIDashboard;
import com.test.toll.utilities.ExcelDrivenTest;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class CommonSteps {

	IGIAdminConsole igiAdminConsolePageResources;
	@Step
	public void enterCredentials(String pageName) throws InterruptedException {
	
		Serenity.sessionVariableCalled("scenarioName").toString();
		String userName = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"User_Name");
		String pass = ExcelDrivenTest.getcellData(Serenity.sessionVariableCalled("scenarioName").toString(),
				"Password");
		if(pageName.equals("IGIAdminConsole")) {
			igiAdminConsolePageResources.login(userName, pass);
		}
	}

}
