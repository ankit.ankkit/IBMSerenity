package com.test.toll.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SecurityAccessManager extends PageObject {

	@FindBy(id = "username")
	public WebElementFacade loginName;

	@FindBy(id = "password")
	public WebElementFacade password;

	@FindBy(xpath = "//div[@class='login-inputs']//input[@value='Login']")
	public WebElement loginbutton;

	@FindBy(xpath = "//img[@src='/pics/iv30.gif']")
	public WebElement homePage;

	@FindBy(xpath = "//div[@id='errId']")
	public WebElement errorMsg;

	@FindBy(xpath = "//div[@class='message-box active']")
	public WebElement verifyingLogoutPage;

	public void login(String userName, String pass) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(this.getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(this.loginName));
		this.loginName.sendKeys(userName.trim());
		this.password.sendKeys(pass.trim());
		this.loginbutton.click();
	}

}
