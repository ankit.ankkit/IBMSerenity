package com.test.toll.pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
//@DefaultUrl("https://google.com")
//@DefaultUrl("https://172.31.9.137/isam/wpm")
import net.serenitybdd.core.pages.WebElementFacade;

public class ISAMRepo extends PageObject {
	@FindBy(xpath="//input[@id='userid']")
	public WebElementFacade HomeloginID;
	
	@FindBy(xpath="//input[@id='password']")
	public WebElementFacade HomeloginPassword;
	
	@FindBy(xpath="//input[@value='Sign On']")
	public WebElementFacade SignOnButton;
	
	@FindBy(xpath = "//td[contains(.,'Task List')]")
	public WebElementFacade Tasklistlbl;
	
	@FindBy(xpath = "//html[@dir='ltr']//tr[2]//td[1]")
	public WebElementFacade SearchUsers;
	

	@FindBy(id="filter")
	public WebElementFacade SearchUserID;
	
	@FindBy(xpath="//input[@value='Search']")
	public WebElementFacade UserIDSearchButton;
	
	@FindBy(id="id")
	public WebElementFacade VerifyUserID;
	
	@FindBy(xpath="//input[@id='accountvalid']")
	public WebElementFacade ChckBoxAccountValid;
	
	
	public void homeloginAndSearch(String homeuserName, String homeuserpassword, String SearchuserID) throws InterruptedException {
/*		WebDriverWait wait = new WebDriverWait(this.getDriver(),20);
		wait.until(ExpectedConditions.elementToBeClickable(this.HomeloginID));*/
		Thread.sleep(5000);
		this.HomeloginID.sendKeys(homeuserName.trim());
		this.HomeloginPassword.sendKeys(homeuserpassword.trim());
		
		this.SignOnButton.click();
		Thread.sleep(2000);
		this.SearchUserID.sendKeys(SearchuserID.trim());
		this.UserIDSearchButton.click();
	}
	
	
	
	
	
	
	
	
	

}
