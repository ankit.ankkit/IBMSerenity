package com.test.toll.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class IGIDashboard extends PageObject {

	@FindBy(xpath = "//span[@class='tab-container']//span[contains(.,'Configure')]")
	public WebElementFacade tab_Configure;

	@FindBy(xpath = "//a[contains(.,'Database Server Configuration')]")
	public WebElementFacade DbServerConfigurationLink;

	@FindBy(xpath = "//td[contains(.,'Identity data store')]")
	public WebElementFacade DbConfigurationDetails;

}
