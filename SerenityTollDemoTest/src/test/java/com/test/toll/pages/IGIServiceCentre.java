package com.test.toll.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class IGIServiceCentre extends PageObject {

	@FindBy(id = "user")
	public WebElementFacade loginName;

	@FindBy(id = "psw")
	public WebElementFacade password;

	@FindBy(id = "other")
	public WebElement loginbutton;

	@FindBy(id = "securityQuestionDropdown1")
	public WebElement SecurityDropDownQuestion1;

	@FindBy(id = "securityQuestionDropdown2")
	public WebElement SecurityDropDownQuestion2;

	@FindBy(id = "securityAnswer1")
	public WebElement SecurityAns1;

	@FindBy(id = "securityAnswer2")
	public WebElement SecurityAns2;

	@FindBy(xpath = "//div[@class='v-slot v-slot-igi_header']//div[@role='button']")
	public List<WebElement> HeaderImages;

	@FindBy(xpath = "//div[@class='popupContent']//div[@class='v-slot']//div[@role='button']")
	public  List<WebElement> changePassword;

	@FindBy(xpath = "//div[@class='popupContent']//span[@class='v-button-wrap']//span[contains(.,'View Self Care Requests')]")
	public WebElement selfCareRequest;

	@FindBy(xpath = "//div[@class='popupContent']//span[@class='v-button-wrap']//span[contains(.,'Password Recovery Setup')]")
	public WebElement passwordRecoverySetup;

	@FindBy(xpath = "//div[@class='popupContent']//span[@class='v-button-wrap']//span[contains(.,'Logout')]")
	public WebElement logOut;
	
	@FindBy(xpath = "//div[@class='v-grid-tablewrapper']//span[@class='v-grid-selection-checkbox']//input[@type='checkbox']")
	public WebElement selectUserForPasswordChange;
	
	@FindBy(xpath = "//span[@class='v-button-wrap']//span[contains(.,'Change Password')]")
	public WebElement changePasButon;
	
	@FindBy(xpath = "//span[@class='v-button-wrap']//span[contains(.,'Cancel and Log Out')]")
	public WebElement cancelAndLogoutButton;
	
	@FindBy(xpath = "//label[contains(.,' Invalid credentials')]")
	public WebElement verifyMessage;
	
	public void login(String userName, String pass) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(this.getDriver(),20);
		wait.until(ExpectedConditions.elementToBeClickable(this.loginName));
		this.loginName.sendKeys(userName.trim());
		this.password.sendKeys(pass.trim());
		this.loginbutton.click();
	}
}
