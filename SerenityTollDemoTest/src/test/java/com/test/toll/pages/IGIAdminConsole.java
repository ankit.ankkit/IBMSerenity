package com.test.toll.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class IGIAdminConsole extends PageObject {

	@FindBy(id = "j_username")
	public WebElementFacade loginName;

	@FindBy(id = "j_password")
	public WebElementFacade password;

	@FindBy(id = "other")
	public WebElement loginbutton;

	@FindBy(xpath = "//span[contains(.,'Logout') and @class='v-button-caption']")
	public WebElement btnLogOut;
	
	
	@FindBy(xpath = "//span[contains(.,'Access Governance Core')and (@class='v-button-caption')]")
	public WebElement lkAccessGovernanceCore;

	@FindBy(xpath = "//div[@class='v-slot']//input[@class='v-textfield v-widget v-has-width Default.TextField v-textfield-Default.TextField']")
	public WebElement textSearchIdentity;

	@FindBy(xpath = "//span[contains(.,'Search') and @class='v-button-caption']")
	public WebElement btnSearch;

	@FindBy(xpath = "//span[@class='v-grid-selection-checkbox']")
	public WebElement chckbxSelectUniqueUser;

	@FindBy(xpath = "//div[@class='v-grid-tablewrapper']//td[@class='v-grid-cell v-grid-cell-focused null null']")
	public WebElement userName;
	
	@FindBy(xpath = "//img[contains(@src,'filter_icon.png')]")
	public WebElement btFilterIcon;
	
	//--------------------New___________________________
	@FindBy(xpath = "//div[@role='tablist']//div[contains(.,'Accounts')and @class='v-captiontext']")
	public WebElement accountsTab;
	
	@FindBy(xpath = "//div[@class='v-slot']//span[@class='v-button-caption' and contains(.,'Monitor')]")
	public WebElement monitorTab;

	@FindBy(xpath = "//div[@class='v-captiontext'and contains(.,'OUT events')]")
	public WebElement outEventsTab;
	
	
	
	@FindBy(xpath = "//div[@class='v-grid-tablewrapper']//td[contains(.,'AD Connector')]")
	public WebElement row_ADConnector;
	
	@FindBy(xpath = "//div[@class='v-grid-tablewrapper']//td[contains(.,'Ideas')]")
	public WebElement row_Ideas;
	
	
	


	public void login(String userName, String pass) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(this.getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(this.loginName));
		this.loginName.sendKeys(userName.trim());
		this.password.sendKeys(pass.trim());
		this.loginbutton.click();
	}
	
	public void logout() throws InterruptedException {
		this.btnLogOut.click();
	}
}
