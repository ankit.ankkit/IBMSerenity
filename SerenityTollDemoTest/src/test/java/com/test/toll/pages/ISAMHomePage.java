package com.test.toll.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

//@DefaultUrl("https://google.com")
//@DefaultUrl("https://172.31.2.234/core/")

/*@NamedUrls(
		  {
		    @NamedUrl(name = "open.issue", url = "http://jira.mycompany.org/issues/{1}")
		  }
		)*/
public class ISAMHomePage extends PageObject {

	@FindBy(id = "login_user_id")
	public WebElementFacade loginName;

	@FindBy(id = "login_password")
	public WebElementFacade password;

	@FindBy(id = "login_submit")
	public WebElement loginbutton;

	@FindBy(xpath = "//div[@class='lotusBanner']//span[@id='prodname']")
	public WebElement productName;

	@FindBy(xpath = "//div[@id='login-box']//label[contains(.,'Login failed')]")
	public WebElementFacade invalidLoginErrorMsg;

	@FindBy(xpath = "//ul[@id='mainnavtab']//li")
	public List<WebElement> tabMenuItems;

	@FindBy(xpath = "//td[text()='Logout']")
	public WebElement logoutButton;

	@FindBy(xpath = "//span[@id='btn-user']//span[contains(@class,'dijitDownArrowButton')]")
	public WebElement adminDropDownButton;

	/*
	 * @WhenPageOpens public void waitUntilIBMSecurityAccessManagerAppears() { //
	 * $(mmtImage).waitUntilVisible();
	 * element(productName).getText().contentEquals("IBM Security Access Manager");
	 * }
	 */

	public void login(String userName, String pass) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(this.getDriver(),20);
		wait.until(ExpectedConditions.elementToBeClickable(this.loginName));
		this.loginName.sendKeys(userName.trim());
		this.password.sendKeys(pass.trim());
		this.loginbutton.click();
	}
}