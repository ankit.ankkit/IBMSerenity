Feature: MyToll - Provisioning

#Background: 
#	Given create user with API for "CreateInternalUser" using "CreateUser" and validate response is "200"
#	
Scenario Outline: MyToll_TC1 :ID-IUC-1_1_Internal User Creation_Request accepted_activation_ successfully
	
	Given new user is on MyToll registration page
	When new cutomer provides input on Self registration page
	
	
	Then IGI receives the request and responds back to MyToll portal indicating acceptance of the request.
	And user MyTOLL custosmer recieves an activation link and Submit password email in order to set password and activate the user.
	Then clicking on set new password and activate the user IGI has receives both the requests and validate both the requests.
	Then IGI finds the requests to be valid and Confirms Acceptance to MyTOLL portal.
	Then IGI sets the users status to active.
	And User account status is set to active in IGI and ISAM ISDS and and a new password is set for the user.
	And User account status in MyToll Database is set as ACTIVE with current modified time stamp and Customer Login to Target toll Successfully. "
	

	Examples:
    | 	ScenarioName 						|
  	|	MyToll_TC1_New User Activation  	| 	

  	
 