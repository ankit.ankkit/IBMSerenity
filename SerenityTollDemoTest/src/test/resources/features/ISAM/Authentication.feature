@Authentication
Feature: Verifying the login with federation and without federation.
		We have used the demo application for the demo purpose

@LoginWithoutFederation
Scenario Outline:  Verify the user login without federation
	Given user navigates to page "https://www.mytoll-app.com/isam/mobile-demo/diag/" for scenario "<ScenarioName>"
	When user enters the username and password
	Then Verify the HTTP Headers
	#And click on button "Logout"
	
    Examples:
      | ScenarioName 	|	
      | Login Without Federation	|	

@LoginWithFederation
Scenario Outline: Verify the user login with federation
	Given user navigates to page "https://www.mytoll-app.com/isam/sps/saml20sp/saml20/logininitial?RequestBinding=HTTPPost&PartnerId=https://www.ibm-idp.com/isam/sps/saml20idp/saml20&NameIdFormat=Email&Target=https://www.mytoll-app.com/isam/mobile-demo/diag/" for scenario "<ScenarioName>"
	When user enters the username and password
	Then Verify the HTTP Headers
	And user logout by using url "https://www.mytoll-app.com/isam/sps/saml20sp/saml20/sloinitial?RequestBinding=HTTPPost"
	Then User session should end
	
	Examples:
  | ScenarioName 						|	
  | Login With Federation			|	

