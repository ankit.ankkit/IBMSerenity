Feature: Verifying Reverse Proxy feature
		#https://172.31.2.212/ 

Scenario Outline:  Verify the login with reverse proxy
	Given user navigates to page "https://172.31.0.249/" for scenario "<ScenarioName>" 
	When user enters "<UserName>" and "<Password>"
	Then Verify "<Result>" for "<Scenario>"
	
    Examples:
      | UserName	|	Password		|	Scenario			|	Result				|
      | toll_user		|	P@ssw0rd		|	PositiveScenario	|	IDM security access manager page should be displayed	|
	  | toll_user		|	admin		|	NegativeScenario	|	Error Message should be displayed		|

Scenario Outline:  Verify the Logout for reverse proxy
	Given user navigates to page "https://172.31.0.249/" for scenario "<ScenarioName>" 
	When user enters "<UserName>" and "<Password>"
	And user logout by using url "https://172.31.0.249/pkmslogout"
	
	Examples:
  | UserName	|	Password		|
  | toll_user		|	P@ssw0rd		|	

