Feature: Verifying the login functionality of ISAM

@LoginToISAM
Scenario Outline:  Verify the access to ISAM portal
	Given user navigates to page "https://172.31.9.55/" for scenario "<ScenarioName>"
	When user enters the username and password
	Then Verify the "<Result>" for "<Scenario>"
	
    Examples:
      | ScenarioName 	|	
      | Succseful Login To IGI		|	


Scenario Outline:  Verify the Logout functonality
	Given user navigates to page "https://172.31.9.55/" for scenario "<ScenarioName>"
	When user enters the username and password
	And click on button "Logout"
	Then User session should end
	
	Examples:
  | ScenarioName 						|	
  | Successful Logout from IGI			|	

