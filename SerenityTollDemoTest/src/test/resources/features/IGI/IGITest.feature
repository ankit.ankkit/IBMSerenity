Feature: Shakeout testing for IGI.Checking te Db instance


@IGI=ShakeoutTesting
Scenario Outline:  Login to IGI Dashboard and verfy the server details
  	Given user navigates to page "https://172.31.32.45:9443/login" for scenario "<ScenarioName>"
	When user enters the username and password
	And click on Configure tab
	Then server details should be displayed
	
	Examples:
	  | ScenarioName 	 				|
	  |IGILogin_PositiveScenario		 |

Scenario Outline: Login with invalid user credentials
	Given user navigates to page "<IGIPage>" for scenario "<ScenarioName>"
	Then user enters username and password for IGI
	And Verify the error message

	Examples:
  | ScenarioName 	| IGIPage	|
  |IGIInvalidLogin 	| https://ip-172-31-42-135.ap-southeast-2.compute.internal:9343/ideas/login.jsp?realm=IDEAS	|	
  
  
	  