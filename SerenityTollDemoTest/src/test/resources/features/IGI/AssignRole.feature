@AssignRole
Feature: Create user test scenario.Creating a new user in IGI 
		and login to IGI dashboard page and IGI service centre

Background: 
	Given create user with API for "CreateInternalUser" using "CreateUser" and validate response is "200"
	
Scenario Outline: Asign Ad role to internal user
	
	Given get user details for user "AutoDemoTest5" using API using "GetUserDetails"
	When Assigning Ad role to internal user
	Then Verify AD role is assigned to the user
	
	Examples:
    | ScenarioName 				|JsonTemplateName		|ResponseCode	|
  	|	CreateInternalUser  	| 	CreateUser			|	200			|
  	|	CreateExternalUser  	| 	CreateUser			|	200			|
  	
  	


  	
 