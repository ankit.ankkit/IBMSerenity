package com.toll.exception;

public class TollExceptions extends RuntimeException {
	
	public TollExceptions(String message, Throwable th) {
		super(message,th);
	}
	
	public TollExceptions(String message) {
		super(message);
	}

	public TollExceptions() {
		super();
	}
	
	public TollExceptions(Throwable th) {
		super(th);
	}
}
