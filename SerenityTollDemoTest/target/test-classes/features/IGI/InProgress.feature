Feature: Create user test scenario.Creating a new user in IGI 
		and login to IGI dashboard page and IGI service centre


@Tania
 Scenario Outline: Get User Details
	Given user navigates to page "https://172.31.9.137/isam/wpm" for scenario "<ScenarioName>"
	When user enters the username and password
	Then Login to security access manager in ISAM
  
	Examples:
  | ScenarioName 		|
  |	Tania				|   
 


@GetUserDetails
 Scenario: Get User Details

 	Given get user details for user "AutoDemoTest5" using API using "GetUserDetails"
	Given Assign Role to user
  


@VerifyUserDetailsOnAdmin
 Scenario Outline:  Verifying user details

 	Given user navigates to page "https://ip-172-31-37-246.ap-southeast-2.compute.internal:9343/ideas/wasLogin.jsp" for scenario "<ScenarioName>"
	When user enters username and password for page "<PageName>"
	#Given Search user on IGIAdminConsole
	Given Verify user details

	Examples:
  | ScenarioName 		|JsonTemplateName	|	PageName			|
  |	verfiyUserDetails 	| 	CreateUser		|	IGIAdminConsole		|

  
  
#  1. NAvigate to IGI admin console
#  2. Login
#  3. Search for user
#  4. Validate user details
  
 
  
  @InProgress2
  Scenario Outline: Debug
  Given Get data From excel "<ScenarioName>"
	Examples:
  | ScenarioName 	 |
  |CreateUser  		 | 	
  
  
    @InProgress3
  Scenario Outline: Disable user
  Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "200"
  And admin send request to disable user
	Examples:
  | ScenarioName 	 |
  |CreateUser  		 | 	
  
 