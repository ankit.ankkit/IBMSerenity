@IGIShakeoutTesting
Feature: Shakeout testing for IGI.Checking te Db instance

Scenario Outline:  Login to IGI Dashboard and verfy the server details
  	Given user navigates to page "https://172.31.34.220:9443/login" for scenario "<ScenarioName>"
	When user enters the username and password
	And click on Configure tab
	Then server details should be displayed
	
	Examples:
	  | ScenarioName 	 				|
	  |IGILogin_PositiveScenario		|


  
  
	  