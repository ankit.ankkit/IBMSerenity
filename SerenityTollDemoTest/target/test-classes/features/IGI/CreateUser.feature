@IDAMCreateUser
Feature: Create user test scenario.Creating a new user in IGI 
		and login to IGI dashboard page and IGI service centre


@CreateUser
Scenario Outline: Successfully internal user creation 
				Verify the create user and Login feature.
				
	Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "200"
	When user navigates to IGI page "<ScenarioName>"
	Then user enters username and password for IGI
	Then user click on Logout
	
	Examples:
    | ScenarioName 				|JsonTemplateName		|ResponseCode	|
  	|	CreateInternalUser  	| 	CreateUser			|	200			|
#  	|	CreateExternalUser  	| 	CreateUser			|	200			|
  	
  	

 Scenario Outline: Invalid login
  					User Login to IGI Service centre page with invalid user credentials
	Given user navigates to IGI page "<ScenarioName>"
	Then user enters username and password for IGI
	And Verify the error message

	Examples:
  | ScenarioName 	|
  |IGIInvalidLogin 	|
  	
 
 
 @DuplicateUser
 Scenario Outline:  creating duplicate user and checking error message
	Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "500"

	Examples:
  | ScenarioName 	|JsonTemplateName	|ResponseCode	|
  |	DuplicateUser 	| 	CreateUser		|	500			|
 
 
 