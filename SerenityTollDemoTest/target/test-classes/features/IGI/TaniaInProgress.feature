Feature: In Progreess items


@Disableuser-Tania
  Scenario Outline: Disable user
  #Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "200"
  And admin send request to disable user
	Examples:
  | ScenarioName 	 	|
  |DisableUser  		 | 

@Enableuser-Tania
  Scenario Outline: enable user
  #Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "200"
  And admin send request to enable user
	Examples:
  | ScenarioName 	 |
  |EnableUser  		 | 	
  
  
  
 @Duplicateemail
Scenario Outline: InSuccessful user creation.Verify the create user and Login feature.
	Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "200"

	Examples:
    | ScenarioName 			|JsonTemplateName		|ResponseCode	|
  	| DuplicateEmailID   	| CreateUser			|	200			|

  
  
   @Mandataryitems_missing_negativescenario_tania
Scenario Outline: Successfully user creation 
				Verify the create user and Login feature.
	Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "500"
#	#When user navigates to IGI page "<ScenarioName>"
#	#Then user enters username and password for IGI
#	Then user click on Logout
	Examples:
    | ScenarioName 				|JsonTemplateName		|ResponseCode	|
  	|	Mantatoryitemsmissing   | 	CreateUser			|	500			|
  #	|	CreateExternalUser  	| 	CreateUser			|	200			|
  
  
	@Incorrect_characters_format_of_attributes_negativescenario_tania
	Scenario Outline: Successfully user creation 
				Verify the create user and Login feature.
	Given create user with API for "<ScenarioName>" using "<JsonTemplateName>" and validate response is "500"
#	#When user navigates to IGI page "<ScenarioName>"
#	#Then user enters username and password for IGI
#	Then user click on Logout
	Examples:
    | ScenarioName 				    |JsonTemplateName		|ResponseCode	|
  	|	Incorrectcharactersformat   | 	CreateUser			|	500			|
  #	|	CreateExternalUser  	    | 	CreateUser			|	200			|
  
 
  