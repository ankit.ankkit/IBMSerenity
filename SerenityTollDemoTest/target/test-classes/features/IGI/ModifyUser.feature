@ModifyUser
Feature: Modify user attributes.

Background: 
	Given create user with API for "CreateInternalUser" using "CreateUser" and validate response is "200"
	
@Modify
 Scenario Outline:  Verify the modify user scenarios.Update the user attributes and verify the details
 					in all target systems
				

	#Given create new user using API for scenario "<ScenarioName>" and using Jason template "CreateUser"
	And user send request to modify his details for scenario "<ScenarioName>" and using Jason template "CreateUser"
#	When user navigates to IGI page
#	Then user enters username and password for IGI
#	Then user click on Logout
	
	Examples:
  | ScenarioName 	 		|ModifyParameter			|	NewValue			|
  |	ModifyUserEmaild 		| 	Emailid					|	ankit@toll.com		|
  |	ModifyUserDOB		 	| 	DOB						|	ankit@toll.com		|
  
 