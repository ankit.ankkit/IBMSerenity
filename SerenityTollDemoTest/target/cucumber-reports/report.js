$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/IGI/TaniaInProgress.feature");
formatter.feature({
  "line": 1,
  "name": "In Progreess items",
  "description": "",
  "id": "in-progreess-items",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 23,
  "name": "InSuccessful user creation.Verify the create user and Login feature.",
  "description": "",
  "id": "in-progreess-items;insuccessful-user-creation.verify-the-create-user-and-login-feature.",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 22,
      "name": "@Duplicateemail"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "create user with API for \"\u003cScenarioName\u003e\" using \"\u003cJsonTemplateName\u003e\" and validate response is \"200\"",
  "keyword": "Given "
});
formatter.examples({
  "line": 26,
  "name": "",
  "description": "",
  "id": "in-progreess-items;insuccessful-user-creation.verify-the-create-user-and-login-feature.;",
  "rows": [
    {
      "cells": [
        "ScenarioName",
        "JsonTemplateName",
        "ResponseCode"
      ],
      "line": 27,
      "id": "in-progreess-items;insuccessful-user-creation.verify-the-create-user-and-login-feature.;;1"
    },
    {
      "cells": [
        "DuplicateEmailID",
        "CreateUser",
        "200"
      ],
      "line": 28,
      "id": "in-progreess-items;insuccessful-user-creation.verify-the-create-user-and-login-feature.;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 68298285,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "InSuccessful user creation.Verify the create user and Login feature.",
  "description": "",
  "id": "in-progreess-items;insuccessful-user-creation.verify-the-create-user-and-login-feature.;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 22,
      "name": "@Duplicateemail"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "create user with API for \"DuplicateEmailID\" using \"CreateUser\" and validate response is \"200\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "DuplicateEmailID",
      "offset": 26
    },
    {
      "val": "CreateUser",
      "offset": 51
    },
    {
      "val": "200",
      "offset": 89
    }
  ],
  "location": "RestServices.createUser(String,String,int)"
});
formatter.result({
  "duration": 5350038707,
  "status": "passed"
});
formatter.after({
  "duration": 158770,
  "status": "passed"
});
});